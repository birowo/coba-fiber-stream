package main

import (
	"os"

	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New()
	app.Get("/", func(ctx *fiber.Ctx) error {
		ctx.Set("Content-Type", "text/html; charset=utf-8")
		ctx.WriteString(`
<video controls>
<source src="/test" type="video/webm" />
</video>
		`)
		return nil
	})
	app.Get("/test", func(ctx *fiber.Ctx) error {
		webm, _ := os.Open("test.webm")
		err := ctx.SendStream(webm)
		if err != nil {
			return err
		}
		return nil
	})
	app.Listen(":8080")
}
